#pragma once
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <time.h>
#include "Rect.h"
#include "app_window.h"
using namespace std;
class RectStash
{
public:
	std::vector <Rect*> rects;
	RectStash(){}
	Rect * first(){ return rects[0]; }
	void add(Rect *r){ rects.push_back(r); }

	void addtorightProportional(){
		static int i = 1;
		static int pl = 3;
		srand(i+pl);
		i += 5+pl;
		pl--;
		float h = rand() % 88 + 20;	//random from 20-88
		h /= 100;

		float r = (float)(rand() % 256) / 256;
		float g = (float)(rand() % 256) / 256;
		float b = (float)(rand() % 256) / 256;

		float xcoor = rects.back()->x + rects.back()->l; //get last x+l coordinate , create there
		Rect *create = new Rect(xcoor + .01, .5, rects.back()->l, h, r, g, b);

		rects.push_back(create);
	}
	void addtoright(){		
		static int i = 1;
		srand( i+3 );
		i += 5;
		float h = rand() % 88 + 20;	//random from 2-88
		h /= 100;					// .2 - .88


		float r = (float)(rand() % 256) / 256;
		float g = (float)(rand() % 256) / 256;
		float b = (float)(rand() % 256) / 256;
		float xcoor = rects.back()->x + rects.back()->l; //get last x+l coordinate , create there
		Rect *create = new Rect(xcoor+.01, .5, .3 , h, r, g, b);

		rects.push_back(create);

		/*
		float h = rand() % 100;	//random from 0-100
		h /= 100;					// 0 - 1

		//stash.add(new Rect(-1, .5, 0.1, 0.3, 0, 1, 0));
		float xcoor;
		Rect *create;
		if (rects.size() != 0)
		{
			xcoor = rects.back()->x + rects.back()->l; //get last x+l coordinate , create there
			create = new Rect(xcoor + .01, .5, .1, h, 1, 1, 1);
		}
		else
			create = new Rect(-1, .5, .1, h, 0, 1, 0);
		rects.push_back(create);
		*/

		}
	void printAreas(std::vector <Rect*> &rects, int inputSize)
	{
		for (int i = 0; i < inputSize; i++)
			std::cout << rects[i]->area << std::endl;
	}
	
	void clear(){
		for (int i = 0; i < this->rects.size(); i++)
		{
			delete rects[i];
		}
	}

	~RectStash(){
		for (std :: vector<Rect*> ::iterator i = rects.begin(); i != rects.end(); i++)
			delete *i;
	}

	void draw(){
		for (auto it = rects.begin(); it != rects.end(); it++)
			(*it)->draw();
	}
};