#pragma once
#include <vector>
#include "Vec.h"
class Rect
{
public:
	std::vector <Vec> eventVecs;
	float x = 0;
	float y = 0;
	float l = 0;
	float h = 0;
	float r = 0;
	float g = 0;
	float b = 0;
	float area = 0;
	bool selected;
	Rect(){
		x = 0;
		y = 0;
		l = .2;
		h = .33;
		area = l*h;
		r = 0;
		g = 1;
		b = 1;		
	}
	Rect(float x, float y, float l, float h, float r, float g, float b){
		this->x = x;
		this->y = y;
		this->l = l;
		this->h = h;
		this->area = (l*h);
		this->r = r;
		this->g = g;
		this->b = b;
	}	

	bool contains(const Vec &v)
	{
		float r_top, r_left, r_bot, r_right;
		if (v.x == 0 && v.y == 0)
			return false;
		r_top = y;
		r_bot = y - h;
		r_left = x;
		r_right = x + l;
		return (r_top >= v.y && r_bot <= v.y && r_left <= v.x && r_right >= v.x);
	}

	void draw(){
		
		glBegin(GL_POLYGON);
		glColor3d(r, g, b);
		glVertex2d(x, y);								//top left
		glVertex2d(x + l, y);						//top right
		glVertex2d(x + l, (y - h) );		//bottom right
		glVertex2d(x, (y - h));					//bottom left
		glEnd();

		/*
		if (selected){
			glLineWidth(3);
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(x, y);				//vertex at top left
			glVertex2d(x + l, y);
			glVertex2d(x + l, y);
			glVertex2d(x + l, y - h);		//vertex at top right
			glVertex2d(x + l, y - h);	//vertex at bottom right
			glVertex2d(x, y - h);			//vertex at bottom left
			glVertex2d(x, y - h);
			glVertex2d(x, y);
			glEnd();
		}
		*/
	}
};

