#pragma once

#include <iostream>
class Vec{
public:
	float x, y;
	static Vec null;

	//default constructor used for null
	Vec(){
		x = 0;
		y = 0; 
	}

	Vec(float x, float y)	
	{
		this->x = x;
		this->y = y;
	}

	void add(Vec in){
		x += in.x;
		y += in.y;

	}
	void print(){
		std::cout << "(" << x << "," << y << ")" << std::endl;
	}
};


