
// Ensure the header file is included only once in multi-file projects
#ifndef APP_WINDOW_H
#define APP_WINDOW_H

# include "glut_window.h"
#include <stdlib.h>
#include "visualc10\RectStash1.h"


// The functionality of your application should be implemented inside AppWindow
class AppWindow : public GlutWindow
 { private :
	 enum MenuEv { evOption0, evOption1, evOption2, evOption3, evOption4, evOption5 };
    float _markx, _marky;
    int _w, _h;

		RectStash stash;
		bool hit;
		float offsetx, offsety;
	
   public :
    AppWindow ( const char* label, int x, int y, int w, int h );
    void windowToScene ( float& x, float &y );

   private : // functions derived from the base class
    virtual void handle ( const Event& e );
    virtual void draw ();
	virtual void idle();
    virtual void resize ( int w, int h );
	int  AppWindow::partition(RectStash &stash, int p, int r);
	void AppWindow::quicksort(RectStash &rects, int p, int inputSize);
 };

#endif // APP_WINDOW_H
