#include <iostream>
#include "app_window.h"
#include <ctime>
#include <time.h>
#include <algorithm>
#include <stdlib.h>
using namespace std;
static int jar = 1;
void AppWindow::idle(){ 
	redraw(); 
}

void insertSort(vector <Rect*> &rects)
{
	float minimum;
	int i;
	if (jar < rects.size())
	{
		minimum = rects[jar]->h;
		int mindex = jar;
		i = jar - 1;
		while (i >= 0 && rects[i]->h > minimum)	//compare areas from j to 0
		{
			swap(rects[i + 1]->r, rects[i]->r);
			swap(rects[i + 1]->b, rects[i]->b);
			swap(rects[i + 1]->g, rects[i]->g);

			rects[i + 1]->h = rects[i]->h;		//original code
			i = i - 1;									//alg code	
		}
		rects[i + 1]->h = minimum;					//alg code
		jar++;
		return;
	}
}



int  AppWindow::partition(RectStash &stash, int p, int r)		//non randomized
{
	float pivot = stash.rects[r]->h;
	int i = p - 1;
	for (int j = p; j <= r-1 ; j++)
	{									
		if (stash.rects[j]->h <= pivot)	
		{
				i = i + 1;									//algs code
				swap(stash.rects[i]->h, stash.rects[j]->h);		//algs code
				//algs code is correct textually, not graphically

			swap(stash.rects[i], stash.rects[j]);				//swap everything
			swap(stash.rects[i]->x, stash.rects[j]->x);				//undo
			swap(stash.rects[i]->h, stash.rects[j]->h);		//undo
		}
		Sleep(350);
		draw();
	}	
	
	swap(stash.rects[i + 1]->h, stash.rects[r]->h);		///algs code
	return i + 1;	
}
void AppWindow::quicksort(RectStash &rects, int p, int inputSize)
{
	int q;
	if (p<inputSize) //more than 1 item
	{
		q = partition(rects, p, inputSize);
		quicksort(rects, p, q - 1);
		quicksort(rects, q + 1, inputSize);
	}
}


int partitionStep(RectStash &stash, int p, int r)		//non randomized
{
	float pivot = stash.rects[r]->h;
	//stash.rects[r]->r = 1; 
	//stash.rects[r]->g = 1; stash.rects[r]->b = 1;

	int i = p - 1;										//-1
	for (int j = p; j <= r - 1; j++)					//we have pivot as last element
	{									
		if (stash.rects[j]->h <= pivot)					//we start at p, which is 0
		{
			//this sorts elements by the value of the pivot
			i = i + 1;											//algs code
			swap(stash.rects[i]->h, stash.rects[j]->h);			//algs code
			
			swap(stash.rects[i], stash.rects[j]);				//swap everything
			swap(stash.rects[i]->x, stash.rects[j]->x);			//undo
			swap(stash.rects[i]->h, stash.rects[j]->h);			//undo
		}
	}
	swap(stash.rects[i + 1]->h, stash.rects[r]->h);		///algs code, swap pivot to correct location

	swap(stash.rects[i + 1], stash.rects[r]);				//swap everything
	swap(stash.rects[i + 1]->x, stash.rects[r]->x);				//undo
	swap(stash.rects[i + 1]->h, stash.rects[r]->h);		///undo
	
	return i + 1;
}
void quicksortStepL(RectStash &rects, int p, int inputSize)
{
	int q;
	if (p<inputSize) //more than 1 item
	{
		q = partitionStep(rects, p, inputSize);
		quicksortStepL(rects, p, q - 1);
		return;
		//quicksortStepL(rects, q + 1, inputSize);
	}
}

void maxHeapify(RectStash &stash, int i, int length)
{
	int l = 2 * i + 1;		//left I(1)	= 2*i
	int r = l + 1;			//right (1) == 2*i +1
	int largest;
	if (l <= length && stash.rects[l]->h > stash.rects[i]->h)
		largest = l;
	else
		largest = i;
	if (r <= length && stash.rects[r]->h > stash.rects[largest]->h)
		largest = r;
	if (largest != i)
	{
		swap(stash.rects[i]->h, stash.rects[largest]->h);
		swap(stash.rects[i], stash.rects[largest]);				//swap everything
		swap(stash.rects[i]->x, stash.rects[largest]->x);				//undo
		swap(stash.rects[i]->h, stash.rects[largest]->h);		//undo

		maxHeapify(stash, largest, length);
	}
}
void buildMaxHeap(RectStash &stash, int inputSize)
{
	int heapSize = inputSize;
	//length-1 down to 0
	for (int i = heapSize; i >= 0; i--)
		maxHeapify(stash, i, inputSize);
}
void heapSort(RectStash &stash, int inputSize)
{
	//input : 0 -> inputSize-1
	buildMaxHeap(stash, inputSize);
	for (int i = inputSize; i >= 0; i--)
	{
		swap(stash.rects[0]->h, stash.rects[i]->h);
		swap(stash.rects[0], stash.rects[i]);				//swap everything
		swap(stash.rects[0]->x, stash.rects[i]->x);				//undo
		swap(stash.rects[0]->h, stash.rects[i]->h);		//undo
		inputSize = inputSize - 1;
		maxHeapify(stash, 0, inputSize);
	}
}


AppWindow::AppWindow ( const char* label, int x, int y, int w, int h )
          :GlutWindow ( label, x, y, w, h )
 {





   _markx = 0;
   _marky = 0;
   addMenuEntry ( "stepping insertSort", evOption0 );
   addMenuEntry(  "full insertSort", evOption1);
   addMenuEntry ( "QuickSort", evOption2 );
   addMenuEntry ( "Heapsort", evOption3);
   addMenuEntry ( "Reset blocks", evOption4);
   addMenuEntry("Reset, 100 block", evOption5);
   srand(time(0));

   stash.add(new Rect(-1, .5, 0.3, 0.55, 0, 1, 0));
   stash.addtoright();//x, y,  l ,  h
   stash.addtoright();
   stash.addtoright();
   stash.addtoright();
   stash.addtoright();

   
   hit = false;
 }

// mouse events are in window coordinates, but your scene is in [0,1]x[0,1],
// so make here the conversion when needed
void AppWindow::windowToScene ( float& x, float &y )
 {
   x = (2.0f*(x/float(_w))) - 1.0f;
   y = 1.0f - (2.0f*(y/float(_h)));
 }

// Called every time there is a window event
void AppWindow::handle(const Event& e)
{
	bool rd = true;
	if (e.type == MouseDown || e.type == Motion)
	{
		_markx = (float)e.mx;
		_marky = (float)e.my;
		windowToScene(_markx, _marky);

		if (!hit&&e.type == MouseDown)
		{
			for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
			{
				(*i)->selected = false;
			}
			//if we click over any part of another rectangle, redraw
			for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
			{
				//be able to select if mouse is down and moving;
				if ((*i)->contains(Vec(_markx, _marky)))
				{
					Rect *temp = *i;
					//(*i)->selected = true;
					stash.rects.erase(i);
					stash.rects.insert(stash.rects.begin(), temp);	//insert (where, val)
					hit = true;
					break;
				}
			}
		}
	}//end mousdown && motion

	if (e.type == Keyboard)
		switch (e.key)
	{
		case ' ': // space bar
			std::cout << "Space pressed.\n";
			_markx = 1.5;
			_marky = 1.5;
			redraw();
			break;
		case 'r': // space bar
			std::cout << "r pressed.\n";
			_markx = 1.5;
			_marky = 1.5;
			stash.clear();
			stash.rects.resize(0);
			stash.add(new Rect(-1, .5, 0.30, 0.55, 0, 1, 0));
			for (int i = 0; i < 5; i++)
				stash.addtoright();//x, y,  l ,  h
			jar = 1;
			redraw();
			break;
		case 'R': // space bar
			std::cout << "R pressed.\n";
			_markx = 1.5;
			_marky = 1.5;
			stash.clear();
			stash.rects.resize(0);
			stash.add(new Rect(-1, .5, 0.0288, 0.55, 0, 1, 0));
			for (int i = 0; i < 50; i++)
				stash.addtorightProportional();
			jar = 1;
			break;

		case 'i':
			insertSort(stash.rects);	//stepping
			break;
		case 'I':						//full insert sort, auto 
		{

			float minimum;
			int i;
			for (int jar = 1; jar < stash.rects.size(); jar++)
			{

				minimum = stash.rects[jar]->h;
				int mindex = jar;
				i = jar - 1;
				while (i >= 0 && stash.rects[i]->h > minimum)	//compare areas from j to 0
				{
					swap(stash.rects[i + 1]->r, stash.rects[i]->r);
					swap(stash.rects[i + 1]->b, stash.rects[i]->b);
					swap(stash.rects[i + 1]->g, stash.rects[i]->g);
					stash.rects[i + 1]->h = stash.rects[i]->h;		//original code=
					i = i - 1;									//alg code	
				}
				stash.rects[i + 1]->h = minimum;					//alg code
				Sleep(350);
				draw();	//call the appwindow draw function	
			}
			break;
		}
		case 'q':
			quicksortStepL(stash, 0, stash.rects.size() - 1);
			break;
		case 'Q':
		{
			quicksort(stash, 0, stash.rects.size() - 1);	
			break;
		}

		case 'h':
			heapSort(stash, stash.rects.size() - 1);
			break;
		case 'H':
		{
			//void heapSort(RectStash &stash, int inputSize){
			buildMaxHeap(stash, stash.rects.size() - 1);
			int inputSize = stash.rects.size() - 1;
			for (int i = inputSize; i >= 0; i--)
			{
				swap(stash.rects[0]->h, stash.rects[i]->h);
				swap(stash.rects[0], stash.rects[i]);				//swap everything
				swap(stash.rects[0]->x, stash.rects[i]->x);				//undo
				swap(stash.rects[0]->h, stash.rects[i]->h);		//undo
				inputSize = inputSize - 1;
				maxHeapify(stash, 0, inputSize);
				Sleep(350);
				draw();	//call the appwindow draw function	

			}
			break;
		}
		case 27: // Esc was pressed
			exit(1);
		}	//end switch


		//if unclick, you are not clicking a rect
		if (e.type == MouseUp)
			hit = false;
		if (e.type == Menu)
		{

			/*
			if (e.menuev == 0)	//stepping insert sort
			{
				insertSort(stash.rects);
			}
			else if (e.menuev == 1)	//full, one-step sort
			{
				float minimum;
				int i;
				
				for (int jar = 1; jar < stash.rects.size(); jar++)
				{
					if (e.key == 27)		//esc pressed
						break;

					minimum = stash.rects[jar]->h;
					int mindex = jar;
					i = jar - 1;
					while (i >= 0 && stash.rects[i]->h > minimum)	//compare areas from j to 0
					{
						swap(stash.rects[i + 1]->r, stash.rects[i]->r);
						swap(stash.rects[i + 1]->b, stash.rects[i]->b);
						swap(stash.rects[i + 1]->g, stash.rects[i]->g);
						stash.rects[i + 1]->h = stash.rects[i]->h;		//original code
						i = i - 1;									//alg code	
					}
					stash.rects[i + 1]->h = minimum;					//alg code
					Sleep(150);
					draw();	//call the appwindow draw function	
					redraw();
				}
			}
			else if (e.menuev == 2)	//one-step quicksort
			{
				quicksort(stash, 0, stash.rects.size() - 1);
			}
			else if (e.menuev == 3)	//heap sort
			{
				heapSort(stash, stash.rects.size() - 1);
			}
			else if (e.menuev == 4)
			{
				stash.clear();
				stash.rects.resize(0);
				stash.add(new Rect(-1, .5, 0.3, 0.55, 0, 1, 0));
				for (int i = 0; i < 5; i++)
					stash.addtoright();
				jar = 1;
			}
			else if (e.menuev == 5)
			{
				stash.clear();
				stash.rects.resize(0);
				stash.add(new Rect(-1, .5, 0.0288, 0.55, 0, 1, 0));
				for (int i = 0; i < 50; i++)
					stash.addtorightProportional();
				jar = 1;
			}
			*/
		}//end menu

		const float incx = 0.02f;
		const float incy = 0.02f;
		if (e.type == SpecialKey)
			switch (e.key)
		{
			case GLUT_KEY_LEFT:  _markx -= incx; break;
			case GLUT_KEY_RIGHT: _markx += incx; break;
			case GLUT_KEY_UP:    _marky += incy; break;
			case GLUT_KEY_DOWN:  _marky -= incy; break;
			default: rd = false; // no redraw
		}

		if (rd) redraw(); // ask the window to be rendered when possible
	}

void AppWindow::resize ( int w, int h )
 {
   // Define that OpenGL should use the whole window for rendering
   glViewport( 0, 0, w, h );
   _w=w; _h=h;
 }

// here we will redraw the scene according to the current state of the application.
void AppWindow::draw ()
 {
   // Clear the rendering window
   glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   // Clear the trasnformation stack
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();

   //draw all rectangles
   for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
   {
	   (*i)->draw();
   } 
   // Swap buffers
   glFlush();         // flush the pipeline (usually not necessary)
   glutSwapBuffers(); // we were drawing to the back buffer, now bring it to the front
}

